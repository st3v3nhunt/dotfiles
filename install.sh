#!/bin/bash

echo "clearing down existing files/folders"

# naff way of preventing errors should files not exist
touch ~/.bashrc
touch ~/.bash_profile
touch ~/.gitconfig
touch ~/.colors
touch ~/.gitprompt
touch ~/.prompt
touch ~/.aliases
touch ~/.pentadactylrc
touch ~/com.googlecode.iterm2.plist
touch ~/.vimrc

# remove existing folders
rm -r ~/.vim

echo "cleaned environment"


echo "starting to symlink"

ln -nfs ~/dotfiles/.bashrc ~/.bashrc
ln -nfs ~/dotfiles/.bash_profile ~/.bash_profile
ln -nfs ~/dotfiles/.gitconfig ~/.gitconfig
ln -nfs ~/dotfiles/.aliases ~/.aliases
ln -nfs ~/dotfiles/.colors ~/.colors
ln -nfs ~/dotfiles/.gitprompt ~/.gitprompt
ln -nfs ~/dotfiles/.prompt ~/.prompt
ln -nfs ~/dotfiles/.pentadactylrc ~/.pentadactylrc
ln -nfs ~/dotfiles/.vimrc ~/.vimrc
ln -nfs ~/dotfiles/.vim ~/.vim
ln -nfs ~/dotfiles/com.googlecode.iterm2.plist ~/com.googlecode.iterm2.plist

echo "finished creating symlinks"
