source ~/.colors
source ~/.aliases
source ~/.gitprompt
source ~/.prompt

# git autocomplete
if [ -f /usr/local/git/contrib/completion/git-completion.bash ]; then
  source /usr/local/git/contrib/completion/git-completion.bash
fi

# rbenv
eval "$(rbenv init -)"
