execute pathogen#infect()
syntax on
set expandtab tabstop=2 shiftwidth=2
set number
set clipboard=unnamed
set colorcolumn=80
